import uuid

import pytz
from django.db import models
from django.db.models import Model


class Mailing(Model):

    start_at = models.DateTimeField(verbose_name='Время начала рассылки')
    content = models.TextField(verbose_name='Текст сообщения')
    tag = models.CharField(max_length=120, verbose_name='Тег')
    end_at = models.DateTimeField(verbose_name='Время окончания рассылки')


class Client(Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField(max_length=11, unique=True, verbose_name='Номер телефона')
    phone_code = models.CharField(max_length=3, verbose_name='Код оператора')
    tag = models.CharField(max_length=80, verbose_name='Тег')
    time_zone = models.CharField(choices=TIMEZONES, max_length=40, default='UTC', verbose_name='Часовой пояс')


class Message(Model):
    sending_time = models.DateTimeField(verbose_name='Время отправки', default=None, null=True)
    status = models.BooleanField(verbose_name='Статус отправки', default=False)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='mlg')
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
