from django.urls import path

from .views import ClientList, ClientDetail, MailingList, MailingDetail


urlpatterns = [
    path('client/', ClientList.as_view()),
    path('client/<int:pk>/', ClientDetail.as_view()),
    path('mailing/', MailingList.as_view()),
    path('mailing/<int:pk>/', MailingDetail.as_view()),
]
