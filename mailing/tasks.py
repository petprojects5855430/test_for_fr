import os
from datetime import datetime
import pytz
import requests

from django.db.models import Q
from mailingservice.celery import app

from .models import Client, Message, Mailing

API_TOKEN = os.environ.get("API_TOKEN")


@app.task(bind=True)
def create_massages(self, dto_dict: dict, mailing_id: int):
    if dto_dict['tag']:
        clients = Client.objects.filter(Q(tag__icontains=dto_dict['tag'])
                                        | Q(phone_code=dto_dict['tag'])).distinct()
    else:
        clients = Client.objects.all()

    mailing = Mailing.objects.get(id=mailing_id)

    for client in clients:
        msg = Message.objects.create(
            mailing=mailing,
            client=client
        )

        if datetime.now().replace(tzinfo=pytz.UTC) < dto_dict['end_at'].astimezone(pytz.UTC):
            send_message.apply_async(kwargs={'msg_id': msg.id,
                                             'phone': client.phone_number,
                                             'text': dto_dict['content'],
                                             'mailing_id': mailing_id,
                                             'end_time': dto_dict['end_at']})
        else:
            send_message.apply_async(kwargs={'msg_id': msg.id,
                                             'phone': client.phone_number,
                                             'text': dto_dict['content'],
                                             'mailing_id': mailing_id,
                                             'end_time': dto_dict['end_at']},
                                     eta=dto_dict['start_at'])


@app.task(bind=True)
def send_message(self, msg_id: int, phone: int, text: str, mailing_id: int, end_time: datetime):
    if end_time.astimezone(pytz.UTC) < datetime.now().replace(tzinfo=pytz.UTC):
        return

    url = f'https://probe.fbrq.cloud/v1/send/{msg_id}'
    headers = {'Authorization': f'Bearer {API_TOKEN}'}
    data = {'id': msg_id, 'phone': phone, 'text': text}

    r = requests.post(url=url, headers=headers, json=data)

    if r.status_code == 200:
        msg = Message.objects.get(id=msg_id)
        msg.status = True
        msg.sending_time = datetime.now()
        msg.save()
