from dataclasses import dataclass, asdict
from datetime import datetime
from typing import Optional

from django.db.models import Model
from mailingservice.celery import app

from .models import Client, Mailing
from .tasks import create_massages


@dataclass
class ClientDTO:
    phone_number: Optional[str]
    phone_code: Optional[str]
    tag: Optional[str]
    time_zone: Optional[str]


@dataclass
class MailingDTO:
    start_at: Optional[datetime]
    content: Optional[str]
    tag: Optional[str]
    end_at: Optional[datetime]


def create_client(dto: ClientDTO) -> Client:
    return Client.objects.create(
        phone_number=dto.phone_number,
        phone_code=dto.phone_code,
        tag=dto.tag,
        time_zone=dto.time_zone
    )


def update_model(dto: ClientDTO, instance: Model) -> Model:
    for field in instance._meta.fields:
        if getattr(dto, field.name, None):
            field = getattr(dto, field.name)
    instance.save()

    return instance


def delete_client(client: Client) -> None:
    revoke_celery_tasks('phone', client.phone_number)

    client.delete()


def create_mailing(dto: MailingDTO) -> Mailing:
    mailing = Mailing.objects.create(
        start_at=dto.start_at,
        content=dto.content,
        tag=dto.tag,
        end_at=dto.end_at
    )
    # создание новых сообщений после создания рассылки
    try:
        create_massages.delay(dto_dict=asdict(dto), mailing_id=mailing.id)
    except create_massages.OperationalError as e:
        print(f'При создании сообщений возникла ошибка: {e}')

    return mailing


def delete_mailing(mailing: Mailing) -> None:
    revoke_celery_tasks('mailing_id', mailing.id)

    mailing.delete()


def revoke_celery_tasks(name: str, value: int) -> None:
    i = app.control.inspect()
    scheduled_tasks = i.scheduled()

    print('\r')
    print(scheduled_tasks)
    print('\r')

    revoke_task_ids = []
    for worker in scheduled_tasks.values():
        for task in worker:
            task_req = task['request']
            if task_req.get('kwargs')[name] == value:
                revoke_task_ids.append(task_req['id'])

    app.control.revoke(revoke_task_ids, terminate=True)
