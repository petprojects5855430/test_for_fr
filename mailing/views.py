from dacite import from_dict
from django.db.models import Count, Q
from rest_framework import generics, response, status

from .serializers import ClientSerializer, MailingSerializer
from .services import ClientDTO, MailingDTO, create_client, \
    create_mailing, update_model, delete_client, delete_mailing
from .models import Client, Mailing


class ClientList(generics.ListCreateAPIView):
    serializer_class = ClientSerializer

    def get_queryset(self):
        return Client.objects.all()

    def post(self, request, *args, **kwargs):
        dto = self._create_dto_from_data(request)
        new_client = create_client(dto)

        return response.Response(self.serializer_class(new_client).data,
                                 status=status.HTTP_201_CREATED)

    def _create_dto_from_data(self, request) -> ClientDTO:
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        return from_dict(data_class=ClientDTO, data=data)


class ClientDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ClientSerializer

    def get_queryset(self):
        return Client.objects.all()

    def delete(self, request, *args, **kwargs):
        client = self.get_object()
        delete_client(client)

        return response.Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        client = self.get_object()
        dto = self._create_dto_from_data(request, client)

        client = update_model(dto, client)
        return response.Response(self.serializer_class(client).data,
                                 status=status.HTTP_200_OK)

    def _create_dto_from_data(self, request, client) -> ClientDTO:
        serializer = self.serializer_class(instance=client, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        return from_dict(data_class=ClientDTO, data=data)


class MailingList(generics.ListCreateAPIView):
    serializer_class = MailingSerializer

    def get_queryset(self):
        return Mailing.objects.prefetch_related('mlg') \
            .annotate(created_msg_num=Count('mlg'),
                      sanded_msg_num=Count('mlg', filter=Q(mlg__status=True)),
                      not_sanded_msg_num=Count('mlg', filter=Q(mlg__status=False)))

    def post(self, request, *args, **kwargs):
        dto = self._create_dto_from_data(request)
        new_mailing = create_mailing(dto)

        return response.Response(self.serializer_class(new_mailing).data,
                                 status=status.HTTP_201_CREATED)

    def _create_dto_from_data(self, request) -> MailingDTO:
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        return from_dict(data_class=MailingDTO, data=data)


class MailingDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = MailingSerializer

    def get_queryset(self):
        return Mailing.objects.prefetch_related('mlg') \
            .annotate(created_msg_num=Count('mlg'),
                      sanded_msg_num=Count('mlg', filter=Q(mlg__status=True)),
                      not_sanded_msg_num=Count('mlg', filter=Q(mlg__status=False)))

    def delete(self, request, *args, **kwargs):
        mailing = self.get_object()
        delete_mailing(mailing)

        return response.Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        mailing = self.get_object()
        dto = self._create_dto_from_data(request, mailing)

        mailing = update_model(dto, mailing)
        return response.Response(self.serializer_class(mailing).data,
                                 status=status.HTTP_200_OK)

    def _create_dto_from_data(self, request, mailing) -> MailingDTO:
        serializer = self.serializer_class(instance=mailing, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        return from_dict(data_class=MailingDTO, data=data)
