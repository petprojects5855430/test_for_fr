from datetime import datetime

import pytz
from rest_framework import serializers
from .models import Mailing, Client, Message
import re


class ClientSerializer(serializers.ModelSerializer):
    """
    Сериализатор клиента.
    """

    PHONE_NUM_RE = r'^7\d{9}$'
    PHONE_CODE_RE = r'^\d{3}$'

    class Meta:
        model = Client
        fields = '__all__'
        read_only_fields = ['id', ]

    def validate(self, data):
        if data.get('phone_number') \
                and not re.match(self.PHONE_NUM_RE, data['phone_number']):
            raise serializers.ValidationError('Неверный формат номер телефона.')
        if data.get('phone_code') \
                and not re.match(self.PHONE_CODE_RE, data['phone_code']):
            raise serializers.ValidationError('Неверный формат кода оператора.')
        return data


class MailingSerializer(serializers.ModelSerializer):
    created_msg_num = serializers.IntegerField(min_value=0, default=0)
    sanded_msg_num = serializers.IntegerField(min_value=0, default=0)
    not_sanded_msg_num = serializers.IntegerField(min_value=0, default=0)

    class Meta:
        model = Mailing
        fields = ['id', 'start_at', 'content', 'tag', 'end_at',
                  'created_msg_num', 'sanded_msg_num', 'not_sanded_msg_num', ]
        read_only_fields = ['id', 'created_msg_num', 'sanded_msg_num', 'not_sanded_msg_num', ]

    def validate(self, data):
        if data.get('end_at') and data['end_at'].astimezone(pytz.UTC) < datetime.now().replace(tzinfo=pytz.UTC):
            raise serializers.ValidationError('Время окончания рассылки прошло.')
        return data
