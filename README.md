# Сервис рассылки для Фабрики Решений

В сервисе реализованны следующие сущности - файл mailig/models.py:
Рассылка (Mailing). Клиент (Client) - получатель рассылки. Сообщение (Message).

Представления - файл mailing/view.py реализованы на основе Generic View классов DRF
Общая информация по энпоинтам в формате OpenAPI доступна по адресу: http://localhost:8000/docs/

В классах сериализаторов - файл mailing/serializers.py реализованна так же простая валидация данных. 
Для Mailing - время окончания рассылки должно быть меньше текущего времени.
Для Client - номер телефона должен состоять из 10 цифр и начинаться с 7, а код оператор должен состоять из 3 цифр.

Бизнес логика рализованна в файле - mailing/services.py. Для обмена данным с представлениями используются dataclasses. 
Бизнес логика реализованна в виде функций.

В файле mailing/tasks.py реализованный задачи celery.

## Логкика работы

После создания рассылки использую эндпонит /mailing/ метод POST создается отложенная задача celery которая запускает
создание сообщений для каждого клиента соответствующего критериям (тег рассылки содержитсья в теге клиента, 
либо тег рассылки совпадает с телефонным кодом клиента). А так же создаются задачи celery которые начнут отправлять
сообщения клиентам в указанное время либо немедленно если время начала рассылки уже прошло.

Функция создания рассылки:

    def create_mailing(dto: MailingDTO) -> Mailing:
        mailing = Mailing.objects.create(
            start_at=dto.start_at,
            content=dto.content,
            tag=dto.tag,
            end_at=dto.end_at
        )
        # создание новых сообщений после создания рассылки
        try:
            create_massages.delay(dto_dict=asdict(dto), mailing_id=mailing.id)
        except create_massages.OperationalError as e:
            print(f'При создании сообщений возникла ошибка: {e}')
    
        return mailing

Функция создания сообщений и задач отправки сообщений:

    @app.task(bind=True)
    def create_massages(self, dto_dict: dict, mailing_id: int):
        if dto_dict['tag']:
            clients = Client.objects.filter(Q(tag__icontains=dto_dict['tag'])
                                            | Q(phone_code=dto_dict['tag'])).distinct()
        else:
            clients = Client.objects.all()
    
        mailing = Mailing.objects.get(id=mailing_id)
    
        for client in clients:
            msg = Message.objects.create(
                mailing=mailing,
                client=client
            )
    
            if datetime.now().replace(tzinfo=pytz.UTC) < dto_dict['end_at'].astimezone(pytz.UTC):
                send_message.apply_async(kwargs={'msg_id': msg.id,
                                                 'phone': client.phone_number,
                                                 'text': dto_dict['content'],
                                                 'mailing_id': mailing_id,
                                                 'end_time': dto_dict['end_at']})
            else:
                send_message.apply_async(kwargs={'msg_id': msg.id,
                                                 'phone': client.phone_number,
                                                 'text': dto_dict['content'],
                                                 'mailing_id': mailing_id,
                                                 'end_time': dto_dict['end_at']},
                                         eta=dto_dict['start_at'])

Фцекция отправки сообщения клиенту:

    @app.task(bind=True)
    def send_message(self, msg_id: int, phone: int, text: str, mailing_id: int, end_time: datetime):
        if end_time.astimezone(pytz.UTC) < datetime.now().replace(tzinfo=pytz.UTC):
            return
    
        url = f'https://probe.fbrq.cloud/v1/send/{msg_id}'
        headers = {'Authorization': f'Bearer {API_TOKEN}'}
        data = {'id': msg_id, 'phone': phone, 'text': text}
    
        r = requests.post(url=url, headers=headers, json=data)
    
        if r.status_code == 200:
            msg = Message.objects.get(id=msg_id)
            msg.status = True
            msg.sending_time = datetime.now()
            msg.save()


## Установка
Перед началом установки необходимо создать в корневой директории файл .env. 
В котором необходимо указать токен внешнего сервиса, в формате:
API_TOKEN=YOUPERSONALTOKEN


Для установки необходимо терминале из директории в которой расположен Dockerfile комманду 

    docker-compose up --build.



После создания контейнеров необходимо выполнить миграцию в базу данных, для этого:
- в терминале необходимо выполнить комманду docker ps
- из списка контейнеров необходимо найти контейнер из образа "mailingservice-web" и выполнить комманду 
    

    docker exec -t -i <имя контейнера> bash


- после чего находясь внутри контейнера docker необходимо выполнить комманду для создания миграций


    python manage.py makemigrations


- и выполнить комманду для запуска миграции

    
    python manage.py migrate


